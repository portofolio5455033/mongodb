import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/database';
import 'firebase/compat/firestore';

// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  apiKey: "AIzaSyD3ZyiOi-C7wwOiGILONmmeTtFoZqdtMIU",
  authDomain: "simple-notes-firebase-32b29.firebaseapp.com",
  projectId: "simple-notes-firebase-32b29",
  storageBucket: "simple-notes-firebase-32b29.appspot.com",
  messagingSenderId: "856166736889",
  appId: "1:856166736889:web:abf512df232f2e3e791328",
  measurementId: "G-GP8S1NGWS3"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const database = firebase.database();
export const googleProvider = new firebase.auth.GoogleAuthProvider();

// Initialize Firebase
// const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

export default firebase;
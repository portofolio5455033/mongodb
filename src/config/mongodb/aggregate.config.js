const Post = require('./post.model')

exports.getPostAggregation = async () => {
    return await Post.aggregate([
        {
            $group: {
                _id: "$published",
                count: {$sum: 1}
            }
        }
    ])
}
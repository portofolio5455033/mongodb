const db = require('../index')
const Post = db.posts

exports.findAll = (req, res) => {
    Post.find()
    .then((result) => {
        res.send(result)
    }).catch((err) => {
        res.status(500).send({
            message: err.message || "Some error while retrieving posts."
        })
    });
}

exports.create = (req, res) => {
    const post = new Post({
        title: req.body.title,
        content: req.body.content,
        published: req.body.published ? req.body.published : false
    })

    post.save(post)
    .then((result) => {
        res.send(result)
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while create post."
        })
    });
}


// menampilkan salah satu data post berdasarkan id nya
exports.findOne = (req, res) => {
    const id = req.params.id
    
    Post.findById(id)
    .then((result) => {
        res.send(result)
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while show post."
        })
    });
}

// membuat update data
exports.update = (req, res) => {
    const id = req.params.id

    Post.findByIdAndUpdate(id, req.body)
    .then((result) => {
        if (!result) {
            res.status(404).send({
                message: "Post not found"
            })
        }
         // Tambahkan kondisi untuk mengubah status published menjadi "yes" jika req.body.published == true
         if (req.body.published) {
            result.published = "yes"
            result.save()
        }

        res.json(
            // message: "Post was updated"
            result
        )
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while update post."
        })
    });
}

// membuat delete
exports.delete = (req, res) => {
    const id = req.params.id

    Post.findByIdAndRemove(id)
    .then((result) => {
        if (!result) {
            res.status(404).send({
                message: "Post not found"
            })
        }

        res.send({
            message: "Post was deleted"
        })
    }).catch((err) => {
        res.status(409).send({
            message: err.message || "Some error while delete post."
        })
    });
}

// membuat aggregasi nya
exports.getPostAggregation = async (req, res) => {
    try {
        const data = await Post.aggregate([
            { $match: { published: 'yes' } },
            {
                $group: {
                    _id: "$published",
                    count: {$sum: 1}
                }
            },
            {
                $project: {
                    _id: 0,
                    count: 1
                }
            }
        ])
        res.json(data)
    } catch (err) {
        res.status(500).send({
            message: err.message || "Some error while retrieving posts aggregation."
        })
    }
}
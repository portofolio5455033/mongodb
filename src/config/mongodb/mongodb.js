const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors());
app.use(express.json())
app.use(express.urlencoded({ extended: true }))


const db = require('./index')
db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: true
    })
    .then(() => {
        console.log('Database connected!')
    }).catch((err) => {
        console.log('Cannot connect to the database!', err)
        process.exit()
    })

// untuk menambahkan url atau router, ('/') = untuk parameternya
// res = respon
app.get('/', (req, res) => {
    res.json({
        message: 'welcome to my website'
    })
})

// agar routes ini bisa digunakan dalam aplikasi nodejs kita, perlu didaftarkan ke file disini
require('./routes/post.routes')(app)

const PORT = 8000
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`)
})
module.exports = (mongoose) => {
    const schema = mongoose.Schema(
        {
            title: String,
            content: String,
            published: {
                type: String,
                default: "no"
            }
        },
        { timestamps: true }
    )
    
    // mengubah objeck _id menjadi objek id saja
    schema.method('toJSON', function() {
        const {__v, _id, ...object} = this.toObject()
        object.id = _id
        return object
    })

    const Post = mongoose.model("posts", schema)
    return Post
}
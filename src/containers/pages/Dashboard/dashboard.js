import React from 'react';
import { Button, Typography, Grid, TextField, Divider, Container, Box } from '@mui/material';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';


import firebase, { googleProvider } from '../../../config/firebase/index';

import { useState, useEffect } from 'react';
import MuiAlert from '@mui/material/Alert';
import Snackbar from '@mui/material/Snackbar';

import { useNavigate } from "react-router-dom";
import axios from "axios";

// table
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

// dialog
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';



// const handlechangesimpan = (data) => {
//   firebase.database().ref('notes/' + data.userId).set({
//     title: data.title,
//     content: data.content,
//     date: data.date
//   });
// };

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export default function Dashboard() {

  const [users, setUsers] = useState([]);
  const [userName, setUserName] = useState("");
  const [showWelcome, setShowWelcome] = useState(false);

  // const [simpan, setSimpan] = useState(false);
  // const [newUser, setNewUser] = useState({ title: '', content: '' })


  useEffect(() => {
    const getUsers = async () => {
      const usersRef = firebase.firestore().collection('users');
      const snapshot = await usersRef.get();
      const usersList = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
      setUsers(usersList);
    };

    getUsers();
  }, []);

  // menyimpan user dilocal storage
  useEffect(() => {
    const displayName = localStorage.getItem('userDisplayName');
    if (displayName) {
      setUserName(displayName);
      setShowWelcome(true);
    }
  }, []);

  const handleCloseSnackbar = () => {
    setShowWelcome(false);
  }


  const navigate = useNavigate();
  // kalau bukan user maka dia akan terpental ke halaman login lagi
  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        navigate('/')
      }
    })
  }, []);

  const handleLogout = () => {
    firebase.auth().signOut();
  };

  
  const [data, setData] = useState([]);
  const [newTitle, setNewTitle] = useState("");
  const [newContent, setNewContent] = useState("");
  const [addedData, setAddedData] = useState(null);

  useEffect(() => {
    fetch('http://localhost:3031/users')
      .then((response) => response.json())
      .then((data) => setData(data));
  }, [addedData]);


  // menghubungkan ke JSON server
  const handleSubmit = (e) => {
    e.preventDefault();

    const newPost = { title: newTitle, content: newContent };
    //  new Date()

    fetch("http://localhost:3031/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newPost),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setNewTitle("");
        setNewContent("");
        setAddedData(data); // memperbarui state addedData dengan data baru
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };


  // penghubung ke mongoDB

  // useEffect(() => {
  //   axios.get('http://localhost:8000/api/posts') // ubah sesuai dengan URL API yang telah Anda konfigurasi
  //     .then(response => {
  //       setUser(response.user);
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     })
  // }, []);

  // untuk menampilkan data
  const [posts, setPosts] = useState([]);

 useEffect(() => {
    axios.get('http://localhost:8000/api/posts')
      .then(response => {
        setPosts(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  // membuat tombol inputannya
  const [id, setId] = useState('');
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [published, setPublished] = useState(false);

  const hendleSimpan = async () => {
    axios.post('http://localhost:8000/api/posts', { title, content })
    .then((response) => {
      console.log(response.data);
      setPosts([...posts, response.data]);
      // Setel ulang bidang input setelah berhasil menambahkan posting
      setTitle('');
      setContent('');
      setPublished(false);
    })
    .catch((error) => {
      console.log(error);
    })
  }

  // membuat tombol delete
  const handleDelete = (id) => {
    axios.delete(`http://localhost:8000/api/posts/${id}`)
      .then(response => {
        // Update the posts state by filtering out the deleted post
        setPosts(posts.filter(post => post.id !== id));
      })
      .catch(error => {
        console.log(error);
      });
  };

  // membuat tombol update
  const [dialogOpen, setDialogOpen] = useState(false);


  const handleUpdate = () => {
    axios.put(`http://localhost:8000/api/posts/${id}`, {title, content, published})
      .then(response => {
        setPosts(posts.map(post => {
          if (post.id === id) {
            return response.data;
          } else {
            return post;
          }
        }));
        setDialogOpen(false);
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleDialogOpen = (post) => {
    setId(post.id);
    setTitle(post.title);
    setContent(post.content);
    setPublished(post.published);
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setId('');
    setTitle('');
    setContent('');
    setPublished(false);
    setDialogOpen(false);
  };


  // membuat aggregasinya
  const handleAggregation = () => {
    axios.get('http://localhost:8000/api/posts/aggregate')
    .then(response => {
      setData(response.data)
      console.log(response.data);
    })
    .catch(error => {
      console.log(error);
    });
};


  return (
    <Box>
      <Button onClick={handleLogout}>Log Out</Button>

      <Grid container direction="column" justifyContent="center" alignItems="center" spacing={2} pt={2}>

        <Grid item>
          <TextField
            sx={{ width: 500 }}
            id="title"
            placeholder="title"
            variant="outlined"
            autoComplete="off"
            // value={newTitle}
            // onChange={(e) => setNewTitle(e.target.value)}
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </Grid>

        <Grid item>
          <TextField
            sx={{ width: 500 }}
            id="outlined-multiline-static"
            placeholder="content"
            multiline
            rows={10}
            // value={newContent}
            // onChange={(e) => setNewContent(e.target.value)}
            value={content}
            onChange={(e) => setContent(e.target.value)}
          />
        </Grid>

        <Grid item>
          <Button variant="outlined" color="success" onClick={hendleSimpan}>SIMPAN</Button>
        </Grid>

        <Container maxWidth='lg'>
          <Divider orientation="horizontal" sx={{ width: '100%', borderColor: 'black', pt: 2 }} />

          <Button variant='contained' color='primary' onClick={handleAggregation}>Run Aggregation</Button>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                <TableCell align='left'>
                    <Typography variant="subtitle1" fontWeight='bold'>ID</Typography>
                  </TableCell>
                  <TableCell align='left'>
                    <Typography variant="subtitle1" fontWeight='bold'>Title</Typography>
                  </TableCell>
                  <TableCell align='center'>
                    <Typography variant="subtitle1" fontWeight='bold'>Content</Typography>
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle1" fontWeight='bold'>Published</Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                  {posts.map(post  => (
                  <TableRow
                    key={post.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row" align='left'>{post.id}</TableCell>
                    <TableCell align="center">{post.title}</TableCell>
                    <TableCell align="center">{post.content}</TableCell>
                    <TableCell align="center">{post.published ? 'Yes' : 'No'}</TableCell>
                    <TableCell>
                      <Button onClick={() => handleDelete(post.id)} color="secondary">
                        Delete
                      </Button>
                    </TableCell>
                    <TableCell>
                    <Button variant="outlined" onClick={() => handleDialogOpen(post)}>
                        Update
                    </Button>
                    <Dialog open={dialogOpen} onClose={handleDialogClose}>
                        <DialogTitle>Update Post</DialogTitle>
                        <DialogContent>
                          <form>
                            <TextField
                              label="Title"
                              variant="outlined"
                              margin="normal"
                              value={title}
                              onChange={(e) => setTitle(e.target.value)}
                            />
                            <TextField
                              label="Content"
                              variant="outlined"
                              margin="normal"
                              multiline
                              rows={4}
                              value={content}
                              onChange={(e) => setContent(e.target.value)}
                            />
                            {/* <FormControlLabel
                              control={
                                <Checkbox
                                  checked={selectedPost.published}
                                  name="published"
                                  onChange={handleInputChange}
                                  color="primary"
                                />
                              }
                              label="Published"
                            /> */}
                          </form>
                        </DialogContent>
                        <DialogActions>
                          <Button onClick={handleDialogClose} color="primary">
                            Cancel
                          </Button>
                          <Button onClick={handleUpdate} color="primary">
                            Update
                          </Button>
                        </DialogActions>
                      </Dialog>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Container>
      </Grid>

      
      <Snackbar open={showWelcome} autoHideDuration={5000} onClose={handleCloseSnackbar}>
        <MuiAlert onClose={handleCloseSnackbar} severity="success">
          Welcome, {userName}!
        </MuiAlert>
      </Snackbar>
    </Box>
  )
}

// import logo from './logo.svg';
// import './App.css';
import React from 'react';
// import Header from '../../../Header/Header';
// import Firebase from '../../../component/firebase';
// import Footer from '../../../Footer/Footer';
// import Appnavbar from './Appnavbar'
import {Router, Route, Link, Routes } from 'react-router-dom';

import "./App.css";
import Dashboard from '../Dashboard/dashboard';
import Login from '../Login/login';
import Register from '../Register/register';

function App() {
  return (
    <div>
      {/* <Header /> */}
      {/* <Firebase /> */}
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/dashboard" element={<Dashboard />} />
      </Routes>
      {/* <Footer /> */}
    </div>
  );
}

export default App;

import React from 'react'
import { Button, Typography, Box, Grid } from '@mui/material'
import TextField from '@mui/material/TextField';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CircularProgress from '@mui/material/CircularProgress';

import { useState, useEffect } from 'react';
import firebase, { googleProvider } from '../../../config/firebase/index';
import { useNavigate } from "react-router-dom";



export default function Register() {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLogin, setIsLogin] = useState(false);
  const navigate = useNavigate();
  
  const handleChangeText = (event) => {
    setEmail(event.target.value);
  };

  const handleChangeText1 = (event) => {
    setPassword(event.target.value);
  };


  const [isLoading, setIsLoading] = useState(false);

  const handleLoginClick = () => {
    setIsLoading(true);
    // console.log(`Email: ${email}`);
    // console.log(`Password: ${password}`);

    firebase.auth().signInWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // Signed in 
        var user = userCredential.user;
        console.log('success: ', user);
        // ...
        navigate("/dashboard");
        // harusnya menggunakan promise juga untuK menvalidasi password nya
       
      })

      .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      console.log(errorCode, errorMessage);
      
    })
    .finally(() => {
      setIsLoading(false);
    });

  };

  // membuat login dengan google
  const handleLoginWithGoogle = () => {
    firebase.auth().signInWithPopup(googleProvider)
    .then((result)=>{
      var credential = result.credential;
      // This gives you a Google Access Token. You can use it to access the Google API.
      var token = credential.accessToken;
      // The signed-in user info.
      
      // var user = result.user;
      // localStorage.setItem('user', JSON.stringify(user));
      
      // IdP data available in result.additionalUserInfo.profile.
      const { displayName } = result.user;
      localStorage.setItem("userDisplayName", displayName);
      navigate("/dashboard");
        // ...
    }).catch((error) => {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
      alert(errorMessage)
    })
  }
  
// kalau dia user makan dia tidak bisa balik ke halaman login 
  useEffect(() => {
    firebase.auth().onAuthStateChanged((user) => {
      if(user){
        navigate('/dashboard')
      }
    })
  }, []);


  return (
    <div>
      <Box sx={{bgcolor: '#9966CC'}}>
        <Grid container direction='column' justifyContent='center' alignItems='center' sx={{minHeight: '100vh'}}>
          <Grid item>
            <Card sx={{ maxWidth: 345 }}>
            <CardContent>
              <Typography color='#1565c0' variant="h6">Login Page</Typography>
              <Box pt={1}>
                <TextField autoComplete="off" sx={{width: 320}} id="email"  placeholder="Email*" variant="outlined" onChange={handleChangeText} />
                <TextField 
                  sx={{paddingTop: 1, width: 320}} 
                  id="password"  
                  placeholder="Password*" 
                  variant="outlined" 
                  type="password" 
                  autoComplete="current-password" 
                  onChange={handleChangeText1}
                  // autoComplete="off"
                />
              </Box>
            </CardContent>
            <CardActions disableSpacing sx={{justifyContent: 'center'}}>
              <Button variant="contained" onClick={handleLoginClick} 
              > 
                {isLoading && <CircularProgress size={24} color="inherit" />}
                {!isLoading ? 'Login' : 'Loading...'}
              </Button>
            </CardActions>
            </Card>
          </Grid>

          <Grid item>
              <Typography align='center' sx={{color: 'white'}}>OR</Typography>
              <Button onClick={handleLoginWithGoogle} variant="outlined" color="inherit" sx={{width: 348, color: 'white'}}>Login dengan Google</Button>
          </Grid>
        </Grid>
      </Box>

    </div>
  )
}
